### Simple package that provides an angularjs-way of linking templates to models

Its usage is pretty straightforward

```
var link = require('template-linker').link;

var saalihou = {name: 'saalihou', age: 5};
var linked = link('Hello [user.name]. In 8 years you will be [user.age + 8].', {user: saalihou});

console.log(linked);

// Hello saalihou. In 8 years you will be 13.
```

If you want to use literal square brackets, you can escape them with the \ character.

Note: beware, in JavaScript, the \ character in itself needs to be escaped. So you would actually write \\\\[ instead of \\[ if your template is a JavaScript string.

Your contribution is welcome :).
