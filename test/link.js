var assert = require('chai').assert;
var link = require('../src/link');
var user;

before(function () {
    user = {name: 'saalihou', age: 18};
});

describe('linking function', function () {
    it('should return the correct built template', function () {
        var linked = link('Hello [user.name]. You are [user.age]', {user: user});
        assert.strictEqual(linked, 'Hello saalihou. You are 18');
    });

    describe('with code', function () {
        it('should return the correct built template', function () {
            var linked = link('Hello [user.name]. The double of your age is [user.age*2]',
                              {user: user}
                             );
            assert.strictEqual(linked, 'Hello saalihou. The double of your age is 36');
        });
    });

    describe('with escaped brackets', function () {
        it ('should not link the escaped expressions', function () {
            var linked = link('Hello \\[user.name\\]');
            assert.strictEqual(linked, 'Hello [user.name]');
        });
    });
});
