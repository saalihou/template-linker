module.exports = function (template, model) {
    var compiler = require('angular-expressions');
    var out = template;

    out.split('[').forEach(function (chunk, index, chunks) {
        // escaped opening bracket, so dont link
        if (chunks[index-1] && chunks[index-1].substr(-1) === '\\') {
            return;
        }
        if (chunk.indexOf(']') !== -1) {
            // there is an expression in here, lets link it
            var expression = chunk.substr(0, chunk.indexOf(']'));
            var link = compiler.compile(expression);
            out = out.replace('[' + expression + ']', link(model));
        }
    });

    out = out.replace(/\\\[/, '[');
    out = out.replace(/\\\]/, ']');

    return out;
}
